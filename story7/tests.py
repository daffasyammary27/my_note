from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from .views import home

# Create your tests here.
class TestHome(TestCase):
    def test_home_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_home_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main.html')